#-*- coding: utf-8 -*-
import requests
import json
import sys

class LibraryApi:
    def upload_file_add_book(files):
        url = 'http://127.0.0.1:8000/api/library/'
        files = {'file': open(files[0:], 'rb')}
        response = requests.post(url, files=files)
        json_data = json.loads(response.text)['detail']
        print json_data

    def upload_file_new_book(files):
        url = 'http://127.0.0.1:8000/api/library/'
        files = {'file': open(files[0:], 'rb')}
        response = requests.patch(url, files=files)
        json_data = json.loads(response.text)['detail']
        print json_data

    def get_book_list():
        url = 'http://127.0.0.1:8000/api/book/'
        response = requests.get(url)
        json_data = json.loads(response.text)
        print json_data

    def create_new_book(title, Ic_classification, name, surname, birthdate):
        url = 'http://127.0.0.1:8000/api/book/'
        book_info = {'title': title, 'cins': Ic_classification, 'name': name,
                    'surname': surname, 'birthdate': birthdate}
        response = requests.post(url, data=book_info)
        json_data = json.loads(response.text)['detail']
        print json_data

    def get_author_list():
        url = 'http://127.0.0.1:8000/api/author/'
        response = requests.get(url)
        json_data = json.loads(response.text)
        print json_data

    def add_new_author(name, surname, birthdate):
        url = 'http://127.0.0.1:8000/api/author/'
        author_info = {'name': name, 'surname': surname, 'birthdate': birthdate}
        response = requests.post(url, data=author_info)
        json_data = json.loads(response.text)['detail']
        print json_data

    def get_book_info(id):
        url = 'http://127.0.0.1:8000/api/book/{}/'.format(int(id))
        response = requests.get(url)
        print response.text

    def update_book_info(id, title, Ic_classification, name, surname, birthdate):
        url = 'http://127.0.0.1:8000/api/book/{}/'.format(int(id))
        book_info = {'title': title, 'cins': Ic_classification, 'name': name,
                'surname': surname, 'birthdate': birthdate}
        response = requests.patch(url, data=book_info)
        json_data = json.loads(response.text)['detail']
        print json_data

    def replace_book_info(id, title, Ic_classification, name, surname, birthdate):
        url = 'http://127.0.0.1:8000/api/book/{}/'.format(int(id))
        book_info = {'title': title, 'cins': Ic_classification, 'name': name,
                'surname': surname, 'birthdate': birthdate}
        response = requests.put(url, data=book_info)
        json_data = json.loads(response.text)['detail']
        print json_data

    def get_author_info(id):
        url = 'http://127.0.0.1:8000/api/author/{}/'.format(int(id))
        response = requests.get(url)
        print response.text

    def update_author_detail(id, name, surname, birthdate):
        url = 'http://127.0.0.1:8000/api/author/{}/'.format(int(id))
        author_info = {'name': name, 'surname': surname, 'birthdate': birthdate}
        response = requests.patch(url, author_info)
        json_data = json.loads(response.text)['detail']
        print json_data

    def replace_author_detail(id, name, surname, birthdate):
        url = 'http://127.0.0.1:8000/api/author/{}/'.format(int(id))
        author_info = {'name': name, 'surname': surname, 'birthdate': birthdate}
        response = requests.put(url, author_info)
        json_data = json.loads(response.text)['detail']
        print json_data

    if __name__ == '__main__':
        if sys.argv[1] == 'create_book':
            create_new_book(*sys.argv[2:])  #Enter book and author info
        elif sys.argv[1] == 'list_book':
            get_book_list()
        elif sys.argv[1] == 'upload_new_file':
            upload_file_add_book(*sys.argv[2:])
        elif sys.argv[1] == 'upload_file_new_book':
            upload_file_new_book(*sys.argv[2:])
        elif sys.argv[1] == 'author_list':
            get_author_list()
        elif sys.argv[1] == 'add_author':
            add_new_author(*sys.argv[2:])
        elif sys.argv[1] == 'get_book_info':
            get_book_info(*sys.argv[2:])
        elif sys.argv[1] == 'update_book_info':
            update_book_info(*sys.argv[2:])
        elif sys.argv[1] == 'replace_book_info':
            replace_book_info(*sys.argv[2:])
        elif sys.argv[1] == 'get_author_info':
            get_author_info(*sys.argv[2:])
        elif sys.argv[1] == 'update_author_detail':
            update_author_detail(*sys.argv[2:])
        elif sys.argv[1] == 'replace_author_detail':
            replace_author_detail(*sys.argv[2:])
