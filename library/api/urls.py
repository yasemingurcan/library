from django.conf.urls import url
from . import views
from api.views import BookExample, FileUploadView, AuthorDetail, BookDetailExample, AuthorDetailExample

urlpatterns = [
    url(r'^library/$', FileUploadView.as_view()),
    url(r'^book/$', BookExample.as_view()),
    url(r'^author/$', AuthorDetail.as_view()),
    url(r'^book/(?P<id>[0-9]+)/$', BookDetailExample.as_view()),
    url(r'^author/(?P<id>[0-9]+)/$', AuthorDetailExample.as_view()),


]
