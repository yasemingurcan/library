from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from api.models import Book, Author

import json
import csv

class FileUploadView(APIView):
    authentication_classes = ()
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        Book.objects.all().delete()
        Author.objects.all().delete()
        file_obj = request.FILES['file']
        for i in file_obj:
            data = i.rstrip()
            data = data.split(',')
            author = Author.objects.create(name=data[2],
                                           surname=data[3],
                                           birthdate=data[4]
                                           )
            author.save()
            book = Book.objects.create(title=data[0],
                                       Ic_classification=data[1],
                                       book_author=author
                                       )
            book.save()
        return Response({'detail': 'Suscesfully update database'})

    def patch(self, request):
        file_obj = request.FILES['file']
        for i in file_obj:
            data = i.rstrip()
            data = data.split(',')
            author = Author.objects.get_or_create(name=data[2],
                                                  surname=data[3],
                                                  birthdate=data[4]
                                                  )
            if author[1] is not True:
                author[0].save()
            book = Book.objects.get_or_create(title=data[0],
                                              Ic_classification=data[1],
                                              book_author=author[0]
                                              )
            if book[1] is not True:
                book[0].save()
        return Response({'detail': 'Book objects create Successfully'})

class BookExample(APIView):
    authentication_classes = ()
    permission_classes = (permissions.AllowAny,)

    def get(self, request, format=None):

        b = {
            'name': 'i.book_author.name',
            'surname': 'i.book_author.surname',
            'birthdate': 'i.book_author.birthdate',
            }
         
        return Response(json.dumps(b))

    def post(self, request, *args):
        title = request.data['title']
        Ic_classification = request.data['cins']
        name = request.data['name']
        surname = request.data['surname']
        birthdate = request.data['birthdate']
        new_author = Author.objects.get_or_create(name=name,
                                                  surname=surname,
                                                  birthdate=birthdate
                                                  )
        new_author[0].save()
        new_book = Book.objects.get_or_create(title=title,
                                              Ic_classification=Ic_classification,
                                              book_author=new_author[0])
        new_book[0].save()
        return Response('New book created Succesfully')


class AuthorDetail(APIView):
    authentication_classes = ()
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        new_author = Author.objects.create(name=request.data['name'],
                                           surname=request.data['surname'],
                                           birthdate=request.data['birthdate'])
        new_author.save()
        return Response({'detail': 'Succesfully create author'})

    def get(self, request):
        author_all = Author.objects.all()
        author_dict = []
        for author in author_all:
            a = {
                'id': author.id,
                'name': author.name,
                'surname': author.surname,
                'birthdate': author.birthdate,
            }
            author_dict.append(a)
        return Response(author_dict)

class BookDetailExample(APIView):

    authentication_classes = ()
    permission_classes = (permissions.AllowAny,)

    def get(self, request, **kwarg):
        id = int(kwarg['id'])
        book_list = {}
        if not Book.objects.filter(id=id).exists():
            return Response("Book does not exist")
        else:
            book_id = Book.objects.get(id=id)
            book_list = {
                        'Title of book': book_id.title,
                        'Book classification': book_id.Ic_classification,
            }
            return Response(book_list)

    def patch(self, request, **kwarg):
        new_title = request.data['title']
        new_Ic = request.data['cins']
        new_name = request.data['name']
        new_surname = request.data['surname']
        new_birtdate = request.data['birthdate']
        if not Book.objects.filter(id=kwarg['id']).exists():
            return Response({'detail': 'no such book!'}.items(), status=400)
        book = Book.objects.get(id=kwarg['id'])
        book.title = new_title
        book.Ic_classification = new_Ic
        book.new_name = new_name
        book.new_surname = new_surname
        book.birthdate = new_birtdate
        book.save()
        return Response({'detail': 'Succesfully updated book details'})

    def put(self, request, **kwarg):
        new_title = request.data['title']
        new_Ic = request.data['cins']
        new_name = request.data['name']
        new_surname = request.data['surname']
        new_birtdate = request.data['birthdate']
        if not Book.objects.filter(id=kwarg['id']).exists():
            return Response({'detail': 'No such Book!'})

        book = Book.objects.get(id=kwarg['id'])
        book.title = new_title
        book.Ic_classification = new_Ic
        book.name = new_name
        book.surname = new_surname
        book.birthdate = new_birtdate
        book.save()
        return Response({'detail': 'Succesfully replaced book details'})

class AuthorDetailExample(APIView):
    authentication_classes = ()
    permission_classes = (permissions.AllowAny,)

    def get(self, request, **kwarg):
        if not Author.objects.filter(id=kwarg['id']).exists():
            return Response({'detail': 'No such author'}.items())
        author = Author.objects.filter(id=kwarg['id'])[0]
        author_list = {
                'Author_name': author.name,
                'Author_surname': author.surname,
                'author_birthdate': author.birthdate,
        }
        return Response(author_list)

    def patch(self, request, **kwarg):
        new_name = request.data['name']
        new_surname = request.data['surname']
        new_birtdate = request.data['birthdate']
        if not Author.objects.filter(id=kwarg['id']).exists():
            print new_name
            print kwarg['id']

            return Response({'detail': 'Not such author'}.items())
        author = Author.objects.get(id=kwarg['id'])
        author.name = new_name
        author.surname = new_surname
        author.birthdate = new_birtdate
        author.save()
        return Response({'detail': 'Succesfully updated author'})

    def put(self, request, **kwarg):
        new_name = request.data['name']
        new_surname = request.data['surname']
        new_birtdate = request.data['birthdate']
        if not Author.objects.filter(id=kwarg['id']).exists():
            return Response({'detail': 'Not such author for replaced'})
        author = Author.objects.get(id=kwarg['id'])
        author.name = new_name
        author.surname = new_surname
        author.birthdate = new_birtdate
        author.save()
        return Response({'detail': 'Succesfully replaces author'})
