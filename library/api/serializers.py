from rest_framework import serializers
from api.models import Author, Book


class AuthorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Author
        fields  = ('name', 'surname', 'birthdate')

class BookSerializer(serializers.ModelSerializer):
    book_author = AuthorSerializer()

    class Meta:
        model = Book
        fields = ('title', 'Ic_classification', 'book_author' )
