from django.db import models

class Author(models.Model):
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)
    birthdate = models.DateField(auto_now=False)


class Book(models.Model):
    title = models.CharField(max_length=50)
    Ic_classification = models.CharField(max_length=30)
    book_author = models.ForeignKey(Author)
