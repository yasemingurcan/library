
1) URL: http://127.0.0.1:8000/api/library/
   POST: python client.py upload_new_file library/book.csv
   PATCH: python client.py upload_file_new_book library/book.csv

2) URL: http://127.0.0.1:8000/api/book/
   GET: python client.py list_book
   POST: python client.py create_book book_name book_cins author_name author_surname author_birthdate

3) URL: http://127.0.0.1:8000/api/author/
   GET: python client.py author_list
   POST: python client.py add_author author_name author_surname author_birthdate

4) URL: http://127.0.0.1:8000/api/book/{id}/
   GET: python client.py get_book_info book_id
   PATCH: python client.py update_book_info book_id book_title book_Ic author_name author_surname author_birthdate
   PUT: python client.py replace_book_info book_id book_title book_Ic author_name author_surname author_birthdate

5) URL: http://127.0.0.1:8000/api/author/{id}/
   GET: python client.py get_author_info author_id
   PATCH: python client.py update_author_detail author_id name surname birthdate
   PUT: python client.py update_author_detail author_id name surname birthdate
